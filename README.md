<h1 align="center">
  <br>
  <a href=""><img src="asset/2brain.png" alt="Markdownify" width="200"></a>
  <br>
  2nd Brain
  <br>
</h1>

<h4 align="center">A markdown system to take note and stay organized</h4>

<p align="center">
  <a href="https://saythanks.io/to/nekotone@gmail.com">
      <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
  </a>
  <a href="https://www.paypal.me/nekotone">
    <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
  </a>
</p>

<p align="center">
  <a href="#key-features">Key Features</a> •
  <a href="#how-to-use">How To Use</a> •
  <a href="#download">Download</a> •
  <a href="#credits">Credits</a> •
  <a href="#support">Support</a> •
  <a href="#license">License</a> •
  <a href="#versioning">Versioning</a> •
  <a href="#maintenance">Maintenance</a>
</p>

## Key Features

## How To Use


> **Note**
> this is note


## Download

You can [download](https://github.com/nekotone/projet/releases/tag/v1.2.0) the latest installable version of 2nd Brain.

## Credits

This system uses the following open source vscodium extension

- [Node.js](https://nodejs.org/)

## Support

<a href="https://www.buymeacoffee.com/nekotone" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/purple_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>

## License

This project is licensed under the [GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) GNU GENERAL PUBLIC LICENSE Version 3 - see the [LICENSE.md](LICENSE.md) file for details

## Versioning

We use [Semantic Versioning](http://semver.org/) for versioning. 

---
## Maintenance

Maintained by NekoTone.

> GitHub [@NekoTone](https://github.com/NekoTone)
> GitLab [@NekoTone](https://gitlab.com/NekoTone)
