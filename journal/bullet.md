[[main]]

# Juillet 

[[20220705]]

# Usage

- [ ] Represents a task.
- [x] Represents a completed task.
- [>] Represents a task migrated forward.
- [<] Represents a task migrated backward.
- [/] Represents a task in progress.
- [-] Represents a dropped task.
- [o] Represents an event.
- Represents a note. Nothing special about it.
  
`!` indicates, e.g., priority, inspiration, etc.
`?` indicates, e.g., waiting for someone or something, unclear, etc.
`*` indicates, e.g., something special about the entry,

- [ ] ! Represents a task.
- [x] ? Represents a completed task.
- [>] * Represents a task migrated forward.
  
